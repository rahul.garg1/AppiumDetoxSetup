// // import wdio from 'wdio.conf';
//
// // jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
// const PORT = 4723;
// // const config = {
// //     platformName: 'iOS',
// //     deviceName: 'iPhone 8',
// //     automationName: 'XCUITest',
// //     app: '../ios/build/Build/Products/Debug-iphonesimulator/sampleAppiumApp.app',
// // };
const wdio = require("webdriverio");
//
const driver = wdio.promiseChainRemote('localhost', PORT);


// beforeAll(async () => {
//     await driver.init(config);
//     await driver.sleep(2000); // wait for app to load
// })
// //
// // test('appium renders', async () => {
// //     expect(await driver.hasElementByAccessibilityId('testview')).toBe(true);
// //     expect(await driver.hasElementByAccessibilityId('notthere')).toBe(false);
// // });
//
//
// describe("Running a sample test", () => {
//     // beforeEach(() => {
//     //     $("~app-root").waitForDisplayed(11000, false)
//     // });
//
//     it("Should increase the count by 1", () => {
//         // $("~increase-count").click();
//         let contexts = driver.currentContext()
//         // driver.context(contexts[1]);
//         console.log("cont",contexts)
//     });
// });

const userName= '//XCUIElementTypeTextField[@name="Username"]';
const password = '//XCUIElementTypeSecureTextField[@name="Password"]';
const login = '~submit';


describe("Running a sample test", () => {


    it("Should increase the count by 1", async() => {
        $(userName).click();
        $(userName).addValue('rahul.garg@gmail.com');

        $(password).click();
        $(password).addValue('12345');
        await driver.hideDeviceKeyboard();
        $(login).click();
    });
});